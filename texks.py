#!/usr/bin/env python3

import sys
import json


from list import List
from task import Task



todo_list = List()
todo_list.name = "My Todo List"
todo_list.addTask("banana ^2023-09-05 @Wegmans #grocery")
todo_list.addTask("apple laptop !1 @online")
todo_list.addTask("strawberry @Costco #grocery")


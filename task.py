#!/usr/bin/env python3


import uuid
import datetime  

"""

^   Due Date
!   Priority
#   List or Tag
@   Location
~   Start Date
*   Repeat
=   Time Estimate
//  note description  

getattr(this_prize, choice)  
for key, val in enumerate(verbs):
  print( str(key) + ": " + str(val) )

"""

class Task:
  """a task on a todo list"""

  attributes = [ 'due', 'priority', 'tag', 'context', 'start_date', 'repeat', 'time_est', 'notes' ]
  char_attr = [ '^', '!', '#', '@', '~', '*', '=', '//' ]

  # -----------------------------------------------------------------
  # Constructor
  # -----------------------------------------------------------------
  def __init__(self):
    self.uuid = uuid.uuid1()
    self.created = datetime.datetime.now().isoformat()
    self.completed = False
    self.title = ""
    # Set all the attributes to false
    for attr in self.attributes:
      setattr(self,attr,False)



  # -----------------------------------------------------------------
  # Parse New Task
  # -----------------------------------------------------------------
  def parse(self,task_string):
    """ parse a new task from the given string """
    parsed_task = task_string
    parts = task_string.split(" ")
    for key, char in enumerate(self.char_attr):
      for p in parts:
        if p[0] == char:
          setattr( self, self.attributes[key], p.replace(char,"") )
          parsed_task = parsed_task.replace(p,"")
    self.title = parsed_task.strip()

  # -----------------------------------------------------------------
  # To JSON
  # -----------------------------------------------------------------
  def toJSON(self):
    output = "{"
    output += '"uuid": "'+str(self.uuid)+'", '
    output += '"created": "'+str(self.created)+'", '
    output += '"title": "'+str(self.title)+'"'
    for attr in self.attributes:
      value = getattr( self, attr )
      if value:
        json_key = attr
        json_value = str( value )
        output += ', "'+json_key+'": "'+json_value+'"'
    return output + "}"

  # -----------------------------------------------------------------
  # ToString
  # -----------------------------------------------------------------
  def __str__(self):
    output = str(self.title)
    output += "\nuuid: " + str(self.uuid)
    output += "\ncreated: " + str(self.created)
    for attr in self.attributes:
      value = getattr( self, attr )
      if value:
        output += "\n"
        output += attr.replace("_"," ").title() + ": " + str( value )
    return output 



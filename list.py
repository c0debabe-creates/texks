#!/usr/bin/env python3

from task import Task

class List:
  """a list of tasks"""

  # -----------------------------------------------------------------
  # Constructor
  # -----------------------------------------------------------------
  def __init__(self):
    self.tasks = []

  # -----------------------------------------------------------------
  # ToString
  # -----------------------------------------------------------------
  def __str__(self):
    printed_list = ""
    l = len(self.tasks)
    i = 1
    for task in self.tasks:
      printed_list += str(task)
      #printed_list += "\n"+(80*"-")
      printed_list += "\n"
      i += 1
    return printed_list

  # -----------------------------------------------------------------
  # Add Task
  # -----------------------------------------------------------------
  def addTask(self, new_task):
    task = Task()
    task.parse(new_task)
    self.tasks.append(task)

  # -----------------------------------------------------------------
  # toJSON: Serialize list to json
  # -----------------------------------------------------------------
  def toJSON(self):
    this_list = '{ "tasks": ['
    task_count = len(self.tasks)
    for index, task in enumerate(self.tasks,start=1):
      this_list += task.toJSON()
      if( index < task_count ):
        this_list += ','
    this_list += '] }'
    return this_list

  # -----------------------------------------------------------------
  # save: save JSON to file
  # -----------------------------------------------------------------
  def save(self):
    try:
      file_contents = self.toJSON()
      file_name = "default-list.json"
      file_mode = "w"
      file_handle = open( file_name, file_mode )
      file_handle.write(file_contents)
      file_handle.close()
    except:
      print( "There was an error while saving the list to file")